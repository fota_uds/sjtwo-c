#include <stdio.h>
#include <string.h>

#include "board_io.h"
#include "delay.h"
#include "lpc40xx.h"

#include "clock.h"
#include "hw_timer.h"
#include "uart.h"

#include "ff.h"
#include "iap.h"

#include "can_bus.h"
#include "isotp.h"

#define DEBUG 0

#if DEBUG
#include <stdio.h>
#define vprintf(f_, ...)                                                                                               \
  do {                                                                                                                 \
    fprintf(stderr, "Debug: ");                                                                                        \
    fprintf(stderr, (f_), ##__VA_ARGS__);                                                                              \
    fprintf(stderr, "\n");                                                                                             \
  } while (0)
#else
#define vprintf(f_, ...) /* NOOP */
#endif

typedef enum {
  _s16_32k = 16,
  _s29_32k = 29,
} sector_e;

static const uint32_t APP_START_ADDRESS = 0x00010000;
static const uint32_t APP_END_ADDRESS = 0x0007FFFF;

#define ECU_ID 0x40
static void flash__write_data(const void *data, size_t data_size_in_bytes, uint32_t flash_write_address);
static void flash__erase_application_flash(void);

static void handle_application_boot(void);
static const unsigned *application_get_entry_function_address(void);
static bool application_is_valid(void);
static void application_execute(void);

static const char *get_status_string(uint8_t status) { return (0 == status) ? "OK" : "ERROR"; }

static const uint16_t CAN_BAUD_RATE = 100;
static const uint16_t CAN_TRANSMIT_QUEUE_SIZE = 100;
static const uint16_t CAN_RECEIVE_QUEUE_SIZE = 100;
static const can__num_e CAN_BUS = can1;

void flash_write(uint8_t *data_buffer, size_t size_in_bytes) {
  static uint32_t file_buffer[32 * 1024 / sizeof(uint32_t)] __attribute__((aligned(256)));
  const size_t flash_write_chunk_size = 1024; // Valid values: 256,512,1024,4096

  uint32_t flash_write_address = APP_START_ADDRESS;
  while (true) {
    memset(file_buffer, 0xFF, sizeof(file_buffer));

    memcpy(file_buffer, data_buffer, size_in_bytes);
    // End of file
    if (!(size_in_bytes > 0)) {
      break;
    }

    vprintf("  Read %u bytes\n", (unsigned)size_in_bytes);
    for (uint32_t address_offset = 0; address_offset < size_in_bytes; address_offset += flash_write_chunk_size) {
      const void *data = (const void *)file_buffer + address_offset;
      flash__write_data(data, flash_write_chunk_size, flash_write_address);
      flash_write_address += flash_write_chunk_size;
    }
  }
}

void send_postive_response_frame(uint16_t can_id) {
  uint8_t response_data[3];
  memset(response_data, 0, 3);
  response_data[0] = 0x01;
  response_data[1] = ECU_ID;
  response_data[2] = 1;
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = can_id + 40;
  isotp_tx(&tx_param, can1, response_data, 3, false);
}

void send_negative_response_frame(uint8_t request_id, uint16_t can_id) {
  uint8_t response_data[3];
  memset(response_data, 0, 3);
  response_data[0] = 0x7f;
  response_data[1] = request_id;
  response_data[2] = 1;
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = can_id + 0x7f;
  isotp_tx(&tx_param, can1, response_data, 3, false);
}

void flash__get_fw_from_can() {
  uint8_t rx_data[1024];
  uint8_t init_data[2];
  int16_t valid_attempt = 255;
  uint32_t frame_count = 0;
  bool validRequest = false;
  isotp_params__s rx_params = {};
  isotp_params__s init_params = {};
  isotp__init(&init_params, &init_data, 2);
  isotp__init(&rx_params, &rx_data, 1024);

  static uint32_t file_buffer[4 * 1024 / sizeof(uint32_t)] __attribute__((aligned(256)));
  const size_t flash_write_chunk_size = 1024; // Valid values: 256,512,1024,4096
  bool is_request_valid = false;
  bool flash_fw = false;
  bool flash_completed = false;
  uint32_t flash_write_address = APP_START_ADDRESS;
  static uint8_t num_attempts = 3;
  static void *data = NULL;
  while (num_attempts-- || flash_fw) {
    can__msg_t can_receive_msg = {};
    while (can__rx(CAN_BUS, &can_receive_msg, 1500)) {
      if (can_receive_msg.msg_id == 405) { // start
        vprintf("Request to erase flash received\n");
        isotp_rx(&init_params, can_receive_msg);
      } else if (can_receive_msg.msg_id == 406) {
        fprintf(stderr, ".");
        isotp_rx(&rx_params, can_receive_msg);
      } else if (can_receive_msg.msg_id == 407) { // complete
        flash_fw = false;
        puts("Flash Complete");
        flash_completed = true;
        break;
        // todo: verify hash
      } else {
        printf("Other can message id = %ld\n", can_receive_msg.msg_id);
      }
      if (init_params.fc_info.fc_flag == COMPLETE) {
        uint8_t requested_ecu_id = init_params.recieve_buffer[1];
        if (requested_ecu_id == ECU_ID)
          is_request_valid = true;
        memset(&init_params, 0, sizeof(init_params));
        if (is_request_valid) {
          validRequest = true;
          puts("Erasing flash memory\n");
          flash__erase_application_flash();
          send_postive_response_frame(405);
          is_request_valid = false;
          flash_fw = true;
          puts("Flashing\n");
        } else {
          puts("Invalid erase flash memory request\n");
          send_negative_response_frame(0x01, 405);
        }
      }
      if (rx_params.fc_info.fc_flag == COMPLETE) {
        if (flash_fw) {
          printf("Flashing Frame %ld\n", frame_count++);
          memset(file_buffer, 0xFF, sizeof(file_buffer));
          uint8_t *temp_fb = (uint8_t *)file_buffer;
          for (size_t i = 0; i < rx_params.data_size; i++) {
            temp_fb[i] = rx_data[i];
          }
          // memcpy(file_buffer, rx_data, rx_params.data_size);
#if (0)
          uint32_t *start_addr = (uint32_t *)(rx_data + 4);
          printf("D=%lx\n", *start_addr);
#endif
          vprintf("  Read %u bytes\n", (unsigned)rx_params.data_size);
          for (uint32_t address_offset = 0; address_offset < rx_params.data_size;
               address_offset += flash_write_chunk_size) {
            vprintf("flash_write_address = 0x%lx\nf", flash_write_address);
            vprintf("flash_write_chunk_size = %d\n", flash_write_chunk_size);
            // vprintf("Debug: file_buffer = %p\n", file_buffer);
            // vprintf("Debug: address_offset = %ld\n", address_offset);
            data = (void *)file_buffer + address_offset;
            vprintf("data = %p\n", data);

            flash__write_data(data, flash_write_chunk_size, flash_write_address);
            flash_write_address += flash_write_chunk_size;
          }
          memset(rx_data, 0, sizeof(rx_data));
          memset(&rx_params, 0, sizeof(rx_params));
          isotp__init(&rx_params, &rx_data, 1024);
          send_postive_response_frame(406);
          vprintf("Postive response frame sent\n");
        } else {
          printf("Not valid request: Application will not be flashed\n");
          return;
        }
      }
      if (!validRequest && valid_attempt <= 0) {
        break;
      }
      if (!validRequest)
        valid_attempt--;
    }
    if (flash_completed)
      break;
  }
}

int main(void) {
  const char *line = "------------------------------";
  // can__msg_t can_receive_msg = {};

  delay__ms(100);
  puts(line);
  puts("---------CAN BOOTLOADER ---------");
  printf("ECU ID = 0x%x\n", ECU_ID);
  puts(line);
  delay__ms(100);
  can__init(CAN_BUS, CAN_BAUD_RATE, CAN_RECEIVE_QUEUE_SIZE, CAN_TRANSMIT_QUEUE_SIZE, NULL, NULL);
  const can_std_id_t slist[] = {
      can__generate_standard_id(can1, 401), can__generate_standard_id(can1, 405), can__generate_standard_id(can1, 406),
      can__generate_standard_id(can1, 407) // 2 entries
  };
  /* const can_std_grp_id_t sglist[] = {
      {can__generate_standard_id(can1, 401), can__generate_standard_id(can1, 405)},
      {can__generate_standard_id(can2, 406), can__generate_standard_id(can2, 407)} // Group 2
  };
  const can_ext_id_t *elist = NULL; // Not used, so set it to NULL
  const can_ext_grp_id_t eglist[] = {
      {can__generate_extended_id(can1, 0x3500), can__generate_extended_id(can1, 0x4500)}}; // Group 1
  */
  can__setup_filter(slist, 4, NULL, 0, NULL, 0, NULL, 0);
  // can__bypass_filter_accept_all_msgs();
  can__reset_bus(CAN_BUS);
  send_postive_response_frame(400);
  flash__get_fw_from_can();

  if (application_is_valid()) {
    send_postive_response_frame(407);
  } else {
    send_negative_response_frame(0x01, 407);
  }

  handle_application_boot();

  return 0;
}

static void flash__write_data(const void *data, size_t data_size_in_bytes, uint32_t flash_write_address) {
  uint8_t status = 0;
  status = Chip_IAP_PreSectorForReadWrite(_s16_32k, _s29_32k);
  if (0 != status) {
    printf("    Prepare sectors: %s (%u)\n", get_status_string(status), (unsigned)status);
  }
  vprintf("Prepare sectors: Success\n");
  status = Chip_IAP_CopyRamToFlash(flash_write_address, (uint32_t *)data, data_size_in_bytes);
  printf("    Write %u bytes to 0x%08lX: %s (%u)\n", data_size_in_bytes, flash_write_address, get_status_string(status),
         (unsigned)status);
  vprintf("Copy Ram 2 flash: Success\n");
  status = Chip_IAP_Compare(flash_write_address, (uint32_t)data, data_size_in_bytes);
  if (0 != status) {
    printf("    Compare %u bytes at 0x%08lX: %s (%u)\n", data_size_in_bytes, flash_write_address,
           get_status_string(status), (unsigned)status);
  }
  vprintf("Comapre Ram: Success\n");
}

static void flash__erase_application_flash(void) {
  uint8_t status = 0;

  printf("Preparing and erasing sectors...\n");

  status = Chip_IAP_PreSectorForReadWrite(_s16_32k, _s29_32k);
  printf("  Prepare sectors: %s (%u)\n", get_status_string(status), (unsigned)status);

  status = Chip_IAP_EraseSector(_s16_32k, _s29_32k);
  printf("  Erased sectors : %s (%u)\n", get_status_string(status), (unsigned)status);

  puts("");
}

static void handle_application_boot(void) {
  if (application_is_valid()) {
    hw_timer__disable(LPC_TIMER__0);
    // TODO: uninit SPI

    puts("  Booting...\n\n");

    // No more printfs after this
    clock__uninit();
    uart__uninit(UART__0);
    board_io__uninit();

    application_execute();
  } else {
    const unsigned *application_entry_point = application_get_entry_function_address();
    printf("Application entry point: %p: %p\n", application_entry_point, (void *)(*application_entry_point));

    while (1) {
      puts("ERROR: Application not valid, hence cannot boot to the application");
      printf("Load to the SD card to re-flash, and reboot this board\n");

      delay__ms(3000);
    }
  }
}

static const unsigned *application_get_entry_function_address(void) { return (unsigned *)(APP_START_ADDRESS + 4); }

static bool application_is_valid(void) {
  const unsigned *app_code_start = application_get_entry_function_address();

  return (*app_code_start >= APP_START_ADDRESS) && (*app_code_start <= APP_END_ADDRESS);
}

static void application_execute(void) {
  // Re-map Interrupt vectors to the user application
  SCB->VTOR = (APP_START_ADDRESS & 0x1FFFFF80);

  // Application code's RESET handler starts at Application Code + 4
  const unsigned *app_code_start = application_get_entry_function_address();

  // Get the function pointer of user application
  void (*user_code_entry)(void) = (void *)*app_code_start;

  // Application flash should have interrupt vector and first entry should be the stack pointer of that application
  const uint32_t stack_pointer_of_application = *(uint32_t *)(APP_START_ADDRESS);
  __set_PSP(stack_pointer_of_application);
  __set_MSP(stack_pointer_of_application);

  user_code_entry();
}
