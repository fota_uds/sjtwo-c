#pragma once

#include "can_bus.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct {
  can__msg_t *memory;
  size_t max_size;
  size_t write_index;
  size_t read_index;
  size_t count;
  size_t size_of_element;
} queue_s;

void queue__init(queue_s *buffer, can__msg_t *memory, size_t no_of_items);
bool queue__add_element(queue_s *buffer, can__msg_t *can_msg);
bool queue__remove_element(queue_s *buffer, can__msg_t *can_msg);

size_t queue__get_data_count(queue_s *buffer);