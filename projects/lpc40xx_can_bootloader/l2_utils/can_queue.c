#include "can_queue.h"

#include <string.h>

void queue__init(queue_s *buffer, can__msg_t *memory, size_t no_of_items) {
  buffer->memory = (can__msg_t *)memory;
  memset(memory, 0, no_of_items * sizeof(can__msg_t));
  buffer->max_size = no_of_items;
  buffer->write_index = 0;
  buffer->read_index = 0;
  buffer->count = 0;
}

bool queue__is_full(queue_s *buffer) { return buffer->count == buffer->max_size; }

bool queue__contains_data(queue_s *buffer) { return buffer->count > 0; }
size_t queue__get_data_count(queue_s *buffer) { return buffer->count; }

size_t queue__get_write_index_and_increment(queue_s *buffer) {
  int buffer_write_index = buffer->write_index;
  buffer_write_index = (buffer_write_index % buffer->max_size) + 1;
  buffer->write_index = buffer_write_index;
  return buffer_write_index - 1;
}

size_t queue__get_read_index_and_increment(queue_s *buffer) {
  int buffer_read_index = buffer->read_index;
  buffer_read_index = (buffer_read_index % buffer->max_size) + 1;
  buffer->read_index = buffer_read_index;
  return buffer_read_index - 1;
}

// Adds a byte to the buffer, and returns true if the buffer had enough space to add the byte
bool queue__add_element(queue_s *buffer, can__msg_t *can_msg) {
  if (!queue__is_full(buffer)) {
    int index = queue__get_write_index_and_increment(buffer);
    memcpy(&(buffer->memory[index]), can_msg, sizeof(can__msg_t));
    buffer->count++;
    return true;
  }
  return false;
}

bool queue__remove_element(queue_s *buffer, can__msg_t *can_msg) {
  if (queue__contains_data(buffer) || buffer->max_size == buffer->count) {
    int index = queue__get_read_index_and_increment(buffer);
    memcpy(can_msg, &(buffer->memory[index]), sizeof(can__msg_t));
    buffer->count--;
    return true;
  }
  return false;
}

#ifdef _QUEUE_TEST_

int main(void) {
  static can__msg_t can_buffer[10] = {};
  queue_s can_queue;
  queue__init(&can_queue, can_buffer, 10);
  can__msg_t data1 = {};
  data1.data.qword = 0x1234567887654321;
  queue__add_element(&can_queue, &data1);
  can__msg_t data2 = {};
  data2.data.qword = 0x23825536;
  queue__add_element(&can_queue, &data2);

  can__msg_t data1_pop;
  queue__remove_element(&can_queue, &data1_pop);
  assert((data1_pop.data.qword == data1.data.qword) && "Data Mismatch");

  can__msg_t data2_pop;
  queue__remove_element(&can_queue, &data2_pop);
  assert((data2_pop.data.qword == data2.data.qword) && "Data Mismatch");

  can__msg_t data3_pop;
  queue__remove_element(&can_queue, &data3_pop);
  assert((!queue__remove_element(&can_queue, &data3_pop)) && "Remove Element fail");

  printf("Hello World\n");
  return 0;
}

#endif