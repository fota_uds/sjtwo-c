#include "periodic_callbacks.h"

#include "board_io.h"
#include "can_handler.h"
#include "gpio.h"
#include "isotp.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */
void periodic_callbacks__initialize(void) {
  can_bus_handler__init();
  // This method is invoked once when the periodic tasks are created
}

void periodic_callbacks__1Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led0());
  gpio__toggle(board_io__get_led1());
  can_bus_handler__reset_if_bus_off();
  if (callback_count % 4 == 0) {
#if (1)
    can_bus_handler__process_all_received_messages();
#else
    uint8_t tx_data[40];
    for (int i = 0; i < 40; i++) {
      tx_data[i] = i + 20;
      // if (i == 4095) {
      //   tx_data[i] = 255;
      // }
    }
    isotp_params__s tx_param = {};
    tx_param.CAN_ID = 401;
    isotp_tx(&tx_param, can1, tx_data, 40, false);
#endif
  }
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led1());

  // Add your code here
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led2());
  // Add your code here
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led3());
  // Add your code here
}