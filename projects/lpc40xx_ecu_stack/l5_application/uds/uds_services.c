#include "uds_services.h"
#include "uds.h"

void uds_service__ecu_reset() {
  printf("\nCalling NVIC_SystemReset\n");
  NVIC_SystemReset(); /* if the reset is successful, bootloader will send a positive response frame */
}