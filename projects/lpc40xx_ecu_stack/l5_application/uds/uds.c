#include "uds.h"
#include "uds_services.h"

#include <string.h>

typedef enum { RESET_SERVICE = 0x01, DATA_BY_ID } UDS_SERVICES;
const uint8_t ECU_ID = 0x20;

void send_positive_response(uint8_t requested_service, uint16_t can_id, uint8_t send_data[], size_t size_of_data);

void send_negative_response_frame(uint8_t request_id, uint16_t can_id);

void uds_service_handler(isotp_params__s *rx_params) {
  uint8_t requested_service = rx_params->recieve_buffer[0];
  uint8_t requested_ecu_id = rx_params->recieve_buffer[1];
  char val[] = "76";
  if (requested_ecu_id != ECU_ID) {
    return;
  }
  switch (requested_service) {
  case RESET_SERVICE:
    uds_service__ecu_reset();
    /*if reset fails*/
    send_negative_response_frame(requested_service, rx_params->CAN_ID);
    break;
  case DATA_BY_ID:
    // TODO: Send postive reponse if data id valid else send negative response

    send_positive_response(requested_service, rx_params->CAN_ID, (uint8_t *)val, strlen(val));
    break;
  default:
    send_negative_response_frame(requested_service, rx_params->CAN_ID);
    break;
  }
}
void send_negative_response_frame(uint8_t request_id, uint16_t can_id) {
  uint8_t response_data[3];
  memset(response_data, 0, 3);
  response_data[0] = 0x7f;
  response_data[1] = request_id;
  response_data[2] = 1;
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = can_id + 0x7f;
  isotp_tx(&tx_param, can1, response_data, 3, false);
}

void send_positive_response(uint8_t requested_service, uint16_t can_id, uint8_t send_data[], size_t size_of_data) {
  uint8_t response_data[size_of_data + 3];
  memset(response_data, 0, size_of_data + 3);
  response_data[0] = requested_service;
  response_data[1] = ECU_ID;
  response_data[2] = 1;
  for (int i = 0; i < size_of_data; i++)
    response_data[i + 3] = send_data[i];
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = can_id + 40;
  isotp_tx(&tx_param, can1, response_data, size_of_data + 3, false);
}
