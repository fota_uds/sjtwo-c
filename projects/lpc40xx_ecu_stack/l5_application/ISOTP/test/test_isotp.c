#include "unity.h"

#include <stdio.h>
#include <string.h>

#include "isotp.c"

#include "Mockcan_bus.h"
#include "Mockdelay.h"

#define MAX_PDU_SIZE 7

isotp_params__s g_message_param = {};

void setUp(void) {}

void tearDown(void) {}

bool canTxStub_called = false;
can__msg_t canTxStub_message = {};

bool canTxStub(can__num_e can, can__msg_t *can_message_ptr, uint32_t timeout_ms, int callback) {
  canTxStub_called = true;
  canTxStub_message = *can_message_ptr;
  return canTxStub_called;
}

bool canTxStub_multi_frame(can__num_e can, can__msg_t *can_message_ptr, uint32_t timeout_ms, int callback) {
  canTxStub_message = *can_message_ptr;
  g_message_param.fc_info.sep_time = 100;

  if (callback == 0) {
    for (int i = 0; i < 6; i++)
      TEST_ASSERT_EQUAL_UINT8(i, canTxStub_message.data.bytes[i + 2]);
    // g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 1) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 6, canTxStub_message.data.bytes[i + 1]);
    // g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 2) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 13, canTxStub_message.data.bytes[i + 1]);
    // g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 3) {
    for (int i = 0; i < 4; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 20, canTxStub_message.data.bytes[i + 1]);
    // g_message_param.fc_info.fc_flag = COMPLETE;
  }
  return true;
}

bool canTxStub_multi_frame_check_block(can__num_e can, can__msg_t *can_message_ptr, uint32_t timeout_ms, int callback) {
  canTxStub_message = *can_message_ptr;
  g_message_param.fc_info.sep_time = 100;
  g_message_param.fc_info.block_size = 2;

  if (callback == 0) {
    for (int i = 0; i < 6; i++)
      TEST_ASSERT_EQUAL_UINT8(i, canTxStub_message.data.bytes[i + 2]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 1) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 6, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 2) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 13, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 3) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 20, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 4) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 27, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 5) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 34, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 6) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 41, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 7) {
    for (int i = 0; i < 4; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 48, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  }
  return true;
}

bool canTxStub_multi_frame_check_block_abort(can__num_e can, can__msg_t *can_message_ptr, uint32_t timeout_ms,
                                             int callback) {
  canTxStub_message = *can_message_ptr;
  g_message_param.fc_info.sep_time = 100;
  g_message_param.fc_info.block_size = 2;

  if (callback == 0) {
    for (int i = 0; i < 6; i++)
      TEST_ASSERT_EQUAL_UINT8(i, canTxStub_message.data.bytes[i + 2]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 1) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 6, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 2) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 13, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = ABORT;
  }
  return true;
}

bool canTxStub_multi_frame_check_block_abort_true(can__num_e can, can__msg_t *can_message_ptr, uint32_t timeout_ms,
                                                  int callback) {
  canTxStub_message = *can_message_ptr;
  g_message_param.fc_info.sep_time = 100;
  g_message_param.fc_info.block_size = 2;

  if (callback == 0) {
    for (int i = 0; i < 6; i++)
      TEST_ASSERT_EQUAL_UINT8(i, canTxStub_message.data.bytes[i + 2]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 1) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 6, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 2) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 13, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 3) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 20, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = ABORT;
  } else if (callback == 4) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 27, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 5) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 34, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = ABORT;
  } else if (callback == 6) {
    for (int i = 0; i < 7; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 41, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  } else if (callback == 7) {
    for (int i = 0; i < 4; i++)
      TEST_ASSERT_EQUAL_UINT8(i + 48, canTxStub_message.data.bytes[i + 1]);
    g_message_param.fc_info.fc_flag = CONTINUE;
  }
  return true;
}

bool canTxStub_multi_abort_frame(can__num_e can, can__msg_t *can_message_ptr, uint32_t timeout_ms, int callback) {
  canTxStub_message = *can_message_ptr;
  g_message_param.fc_info.sep_time = 100;

  if (callback == 0) {
    for (int i = 0; i < 6; i++)
      TEST_ASSERT_EQUAL_UINT8(i, canTxStub_message.data.bytes[i + 2]);
    g_message_param.fc_info.fc_flag = ABORT;
  }
  return true;
}

void test__isotp_tx_single_mode(void) {
  uint8_t data[MAX_PDU_SIZE] = {1, 2, 3, 4, 5};
  can__tx_StubWithCallback(canTxStub);
  isotp_params__s message_param = {};

  isotp_tx(&message_param, can1, (void *)&data, 5, false);
  TEST_ASSERT_TRUE(canTxStub_called);
  TEST_ASSERT_EQUAL_UINT8(5, canTxStub_message.data.bytes[0]);
  for (int i = 1; i < 6; i++) {
    TEST_ASSERT_EQUAL_UINT8(i, canTxStub_message.data.bytes[i]);
  }
}

void test__isotp_tx_fc_mode(void) {
  can__tx_StubWithCallback(canTxStub);
  isotp_params__s message_param = {};
  message_param.fc_info.fc_flag = CONTINUE;
  message_param.fc_info.block_size = 5;
  message_param.fc_info.sep_time = 100;

  isotp_tx(&message_param, can1, (void *)&(message_param.fc_info), 5, true);

  TEST_ASSERT_TRUE(canTxStub_called);
  TEST_ASSERT_EQUAL_UINT8(0x30, canTxStub_message.data.bytes[0]);
  TEST_ASSERT_EQUAL_UINT8(5, canTxStub_message.data.bytes[1]);
  TEST_ASSERT_EQUAL_UINT8(100, canTxStub_message.data.bytes[2]);

  message_param.fc_info.fc_flag = WAIT;
  isotp_tx(&message_param, can1, (void *)&(message_param.fc_info), 5, true);

  TEST_ASSERT_TRUE(canTxStub_called);
  TEST_ASSERT_EQUAL_UINT8(0x31, canTxStub_message.data.bytes[0]);
  TEST_ASSERT_EQUAL_UINT8(5, canTxStub_message.data.bytes[1]);
  TEST_ASSERT_EQUAL_UINT8(100, canTxStub_message.data.bytes[2]);
}

void test__isotp_tx_multi_mode_complete(void) {
  uint8_t data[24] = {};
  for (int test_i = 0; test_i < 24; test_i++) {
    data[test_i] = test_i;
  }
  can__tx_StubWithCallback(canTxStub_multi_frame);
  delay__us_Ignore();
  memset(&g_message_param, 0, sizeof(g_message_param));
  isotp_tx(&g_message_param, can1, (void *)&data, 24, false);
}

void test__isotp_tx_multi_mode_block(void) {
  uint8_t data[54] = {};
  for (int test_i = 0; test_i < 52; test_i++) {
    data[test_i] = test_i;
  }
  can__tx_StubWithCallback(canTxStub_multi_frame_check_block);
  delay__us_Ignore();
  memset(&g_message_param, 0, sizeof(g_message_param));
  TEST_ASSERT_TRUE(isotp_tx(&g_message_param, can1, (void *)&data, 52, false));
}

void test__isotp_tx_multi_mode_block_abort(void) {
  uint8_t data[54] = {};
  for (int test_i = 0; test_i < 52; test_i++) {
    data[test_i] = test_i;
  }
  can__tx_StubWithCallback(canTxStub_multi_frame_check_block_abort);
  delay__us_Ignore();
  memset(&g_message_param, 0, sizeof(g_message_param));
  TEST_ASSERT_FALSE(isotp_tx(&g_message_param, can1, (void *)&data, 52, false));
}

void test__isotp_tx_multi_mode_block_abort_true(void) {
  uint8_t data[54] = {};
  for (int test_i = 0; test_i < 52; test_i++) {
    data[test_i] = test_i;
  }

  can__tx_StubWithCallback(canTxStub_multi_frame_check_block_abort_true);
  delay__us_Ignore();
  memset(&g_message_param, 0, sizeof(g_message_param));
  TEST_ASSERT_TRUE(isotp_tx(&g_message_param, can1, (void *)&data, 52, false));
}

void test__isotp_tx_multi_mode_abort(void) {
  uint8_t data[24] = {};
  for (int test_i = 0; test_i < 24; test_i++) {
    data[test_i] = test_i;
  }
  can__tx_StubWithCallback(canTxStub_multi_abort_frame);
  delay__ms_Ignore();
  TEST_ASSERT_FALSE(isotp_tx(&g_message_param, can1, (void *)&data, 24, false));
}

void test__isotp_rx_single_mode(void) {
  uint8_t data[MAX_PDU_SIZE] = {};
  isotp_params__s message_param = {};
  // message_param.recieve_buffer = data;
  // message_param.recieve_buffer_size = MAX_PDU_SIZE;
  isotp__init(&message_param, data, MAX_PDU_SIZE);
  can__msg_t msg_data = {};
  msg_data.msg_id = 101;
  msg_data.data.bytes[0] = 7;
  msg_data.data.bytes[1] = 2;
  msg_data.data.bytes[2] = 3;
  msg_data.data.bytes[3] = 4;
  msg_data.data.bytes[4] = 5;
  msg_data.data.bytes[5] = 6;
  msg_data.data.bytes[6] = 7;
  msg_data.data.bytes[7] = 8;

  // can__rx_ExpectAndReturn(can1, NULL, 0, true);
  // can__rx_IgnoreArg_can_message_ptr();
  // can__rx_ReturnThruPtr_can_message_ptr(&msg_data);
  isotp_rx(&message_param, msg_data);
  TEST_ASSERT_EQUAL_UINT8(COMPLETE, message_param.fc_info.fc_flag);
  for (int i = 0; i < 7; i++)
    TEST_ASSERT_EQUAL_UINT8(i + 2, message_param.recieve_buffer[i]);
}

void test__isotp_rx_consecutive_frame(void) {
  uint8_t data[24] = {};
  isotp_params__s message_param = {};
  // message_param.recieve_buffer = data;
  // message_param.recieve_buffer_size = 24;

  isotp__init(&message_param, data, 24);
  message_param.data_size = 7;
  can__msg_t msg_data = {};
  size_t total_data = 7u;
  const uint32_t test_data = 1;
  msg_data.msg_id = 101;
  msg_data.data.bytes[0] = (2 << 4) | 1;
  for (int i = 0; i < 7; i++) {
    msg_data.data.bytes[i + 1] = (test_data + i);
  }
  // can__rx_ExpectAndReturn(can1, NULL, 0, true);
  // can__rx_IgnoreArg_can_message_ptr();
  // can__rx_ReturnThruPtr_can_message_ptr(&msg_data);
  isotp_rx(&message_param, msg_data);
  // TEST_ASSERT_EQUAL_UINT8(COMPLETE, message_param.fc_info.fc_flag);
  for (int i = 0; i < 7; i++)
    TEST_ASSERT_EQUAL_UINT8(test_data + i, message_param.recieve_buffer[i]);
}

void test__isotp_rx_multi_mode_24_bytes(void) {

  uint8_t data[24] = {};
  can__msg_t msg_data = {};
  isotp_params__s message_param = {};
  size_t total_data = 24u;
  uint8_t cs_8b = 0;
  uint32_t test_data = 1u;
  uint8_t data_bytes_transmit = 7;
  isotp__init(&message_param, data, 24);
  uint16_t no_data_blocks = ((total_data - 6) / 7) + 1;
  if ((total_data - 6) % 7) {
    no_data_blocks++;
  }
  msg_data.msg_id = 101;
  for (int test_frame = 0; test_frame < no_data_blocks; test_frame++) {
    if (test_frame == 0) { // First Frame
      msg_data.data.bytes[0] = ((uint8_t)0x0F & total_data);
      msg_data.data.bytes[0] |= (1 << 4);
      msg_data.data.bytes[1] = (0xFF0 & total_data) >> 4;
      ;
      for (int i = 0; i < 6; i++) {
        msg_data.data.bytes[i + 2] = test_data++;
      }
      total_data -= 6;
      canTxStub_called = false;
      memset(&canTxStub_message, 0, sizeof(canTxStub_message));
      can__tx_StubWithCallback(canTxStub);
    } else { // Consecutive frame
      msg_data.data.bytes[0] = (2 << 4) | ((test_frame)&0x0f);

      if (total_data > 0 && total_data < 7) {
        data_bytes_transmit = total_data;
      }
      for (int i = 0; i < data_bytes_transmit; i++) {
        printf("Test Data = %d\n", test_data);
        msg_data.data.bytes[i + 1] = test_data++;
      }
      total_data -= 7;
    }

    // can__rx_ExpectAndReturn(can1, NULL, 0, true);
    // can__rx_IgnoreArg_can_message_ptr();
    // can__rx_ReturnThruPtr_can_message_ptr(&msg_data);
    isotp_rx(&message_param, msg_data);
    // TEST_ASSERT_EQUAL_UINT8(CONTINUE, message_param.fc_info.fc_flag);
    if (test_frame == 0) {
      TEST_ASSERT_TRUE(canTxStub_called);
      TEST_ASSERT_EQUAL_UINT8(0x30, canTxStub_message.data.bytes[0]);
      TEST_ASSERT_EQUAL_UINT8(0, canTxStub_message.data.bytes[1]);
      TEST_ASSERT_EQUAL_UINT8(0xF1, canTxStub_message.data.bytes[2]);
    }
  }

  for (int test_i = 0; test_i < 24; test_i++) {
    TEST_ASSERT_EQUAL_UINT8(test_i + 1, data[test_i]);
  }
}
