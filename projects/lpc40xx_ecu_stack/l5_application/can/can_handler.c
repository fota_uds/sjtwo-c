#include <stdio.h>
#include <string.h>

#include "can_bus.h"
#include "isotp.h"
#include "uds.h"

// Constants for CAN BUS
static const uint16_t CAN_BAUD_RATE = 100;
static const uint16_t CAN_TRANSMIT_QUEUE_SIZE = 100;
static const uint16_t CAN_RECEIVE_QUEUE_SIZE = 100;
static const can__num_e CAN_BUS = can1;

void can_bus_handler__init(void) {
  can__init(CAN_BUS, CAN_BAUD_RATE, CAN_RECEIVE_QUEUE_SIZE, CAN_TRANSMIT_QUEUE_SIZE, NULL, NULL);
  can__bypass_filter_accept_all_msgs();
  can__reset_bus(CAN_BUS);
}

void can_bus_handler__reset_if_bus_off(void) {
  if (can__is_bus_off(CAN_BUS)) {
    can__reset_bus(CAN_BUS);
    printf("Bus off\n");
  }
}

void can_bus_handler__process_all_received_messages(void) {
  can__msg_t can_receive_msg = {};
  static const int num_services = 2;
  static const int uds_id_start = 400;
  uint8_t rx_data[num_services]
                 [8]; // for UDS in our case the data will never exceed 8 bytes or single frame transmission
  isotp_params__s rx_params[num_services];
  for (int i = 0; i < num_services; i++)
    isotp__init(&rx_params[i], &rx_data[i], 8);
  while (can__rx(CAN_BUS, &can_receive_msg, 0)) {
    // printf("Received\n");
    if (can_receive_msg.msg_id >= uds_id_start &&
        can_receive_msg.msg_id < (num_services + uds_id_start)) { // UDS message range
      // printf("rcvd\n");
      isotp_rx(&rx_params[can_receive_msg.msg_id - uds_id_start], can_receive_msg);
    }
    // printf("data size = %d\n", rx_params.data_size);
    if (rx_params[can_receive_msg.msg_id - uds_id_start].fc_info.fc_flag == COMPLETE) {
      // printf("compt\n");
      uds_service_handler(&rx_params[can_receive_msg.msg_id - uds_id_start]);
      // for (int i = 0; i < rx_params[can_receive_msg.msg_id - uds_id_start].data_size; i++)
      //   printf("D=%d\n", rx_data[can_receive_msg.msg_id - uds_id_start][i]);
      memset(rx_data[can_receive_msg.msg_id - uds_id_start], 0, sizeof(rx_data[can_receive_msg.msg_id - uds_id_start]));
      memset(&rx_params[can_receive_msg.msg_id - uds_id_start], 0,
             sizeof(rx_params[can_receive_msg.msg_id - uds_id_start]));
    }
  }
  // }
}

bool can_bus_handler__send_message(void *argument, uint32_t message_id, const uint8_t bytes[8], uint8_t dlc) {
  can__msg_t send_msg = {};
  send_msg.msg_id = message_id;
  send_msg.frame_fields.data_len = dlc;
  memcpy(send_msg.data.bytes, bytes, dlc);
  return can__tx(CAN_BUS, &send_msg, 0);
}
